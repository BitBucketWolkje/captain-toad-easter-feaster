﻿using Input;
using UnityEngine;

namespace Game
{
    public class RotateCommand : IDirectionalCommand
    {
        private PlayerBehaviour _player;
        private Camera _camera;

        public RotateCommand(PlayerBehaviour player, Camera camera)
        {
            _player = player;
            _camera = camera;
        }

        public void Execute(Vector2 direction)
        {

            Vector3 cameraForward = Vector3.Scale(_camera.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 cameraRight = Vector3.Scale(_camera.transform.right, new Vector3(1, 0, 1)).normalized;

            Vector3 worldDirectionRight = cameraRight * direction.x + cameraForward * direction.y;

            _player.Rotate(worldDirectionRight);
        }
    }

    public class MoveCommand : IDirectionalCommand
    {
        private PlayerBehaviour _player;
        private Camera _camera;

        public MoveCommand(PlayerBehaviour player, Camera camera)
        {
            _player = player;
            _camera = camera;
        }

        public void Execute(Vector2 direction)
        {
            Vector3 cameraForward = Vector3.Scale(_camera.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 cameraRight = Vector3.Scale(_camera.transform.right, new Vector3(1, 0, 1)).normalized;

      
            Vector3 worldDirection  = cameraRight * direction.x + cameraForward * direction.y;

            _player.Move(worldDirection);
        }
    }



    public class JumpCommand : IImpulseCommand
    {
        private PlayerBehaviour _player;
        public JumpCommand(PlayerBehaviour player)
        {
            _player = player;
        }

        public void Execute()
        {
            _player.Jump();
        }
    }

    public class InteractCommand : IImpulseCommand
    {
        private PlayerBehaviour _player;
        public InteractCommand(PlayerBehaviour player)
        {
            _player = player;
        }

        public void Execute()
        {
            _player.Interact();
        }
    }
}