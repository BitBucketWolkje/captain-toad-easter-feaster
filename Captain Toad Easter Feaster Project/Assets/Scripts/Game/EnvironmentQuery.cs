﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;


namespace Game
{
    [Serializable]
    public class EnvironmentQueryConfiguration
    {

        public LayerMask GroundLayerMask = 0;

        [HideInInspector] public float Radius = 1.0f;
        [HideInInspector] public float RayCastHeight;
    }

    /// <summary>
    /// EnvironmentQuery class encapsulates all info available on the surrounding.
    /// Call Update once per frame to update the info before accessing the data.
    /// </summary>
    public class EnvironmentQuery
    {
        private Vector3 _groundNormal;

        private EnvironmentQueryConfiguration _configuration;

        public Vector3 GroundNormal => _groundNormal;

        public EnvironmentQuery(EnvironmentQueryConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Update(Vector3 center)
        {
            var groundPoint = FindGroundPoint(center);

            _groundNormal = FindGroundNormal(groundPoint, center);
        }

        private Vector3 FindGroundPoint(Vector3 center)
        {
            RaycastHit hitInfo;

            var groundPointRay = new Ray(center, Vector3.down);
            if (Physics.SphereCast(groundPointRay, _configuration.Radius, out hitInfo,
                _configuration.RayCastHeight, _configuration.GroundLayerMask))
            {
                return hitInfo.point;
            }

            return Vector3.zero;
        }

        private Vector3 FindGroundNormal(Vector3 groundPoint, Vector3 center)
        {

            RaycastHit hitInfo;

            var groundNormalRay = new Ray(new Vector3(groundPoint.x, center.y, groundPoint.z), Vector3.down);
            if (Physics.Raycast(groundNormalRay, out hitInfo, _configuration.RayCastHeight))
            {
                return hitInfo.normal;
            }

            return Vector3.up;
        }

        private void FindInteractive()
        {
            //var halfExtents = new Vector3(_controller.radius * 0.5f,
            //    _controller.height / 2.0f, _controller.radius * 1.0f);
            //
            //var forwardOffset = transform.forward * halfExtents.x;
            //var upOffset = transform.up * halfExtents.y;
            //
            //var interactiveCenter = transform.position + forwardOffset + upOffset;
            //
            //var interactiveLayer = LayerMask.GetMask("Interactive");
            //
            //var interactives =
            //    Physics.OverlapBox(interactiveCenter, halfExtents, transform.rotation, interactiveLayer);
            //
            //if (interactives.Length > 0)
            //{
            //    var physics = interactives[0].GetComponentInParent<Rigidbody>();
            //    Vector3 direction = interactives[0].transform.position - transform.position;
            //    direction.y = 0;
            //    physics.AddForce(direction * 8.0f, ForceMode.Impulse);
            //}
        }
    }
}