﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Game;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Game
{
    public class PlayerBehaviour : MonoBehaviour
    {
        [SerializeField] private MotorConfiguration _motorConfiguration;

        [SerializeField] private EnvironmentQueryConfiguration _environmentQueryConfiguration;


        private CharacterController _controller;

        private Motor _motor;
        private EnvironmentQuery _environmentQuery;

        private bool _jump = false;
        private bool _interact = false;
        private Vector3 _rotation = Vector3.zero;
        private Vector3 _movement = Vector3.zero;

        void Start()
        {
            _controller = GetComponent<CharacterController>();

            _motor = new Motor(_controller, _motorConfiguration);


            var characterRayCastHeight = _controller.height / 2 + _controller.skinWidth * 2;

            _environmentQueryConfiguration.RayCastHeight = characterRayCastHeight;
            _environmentQueryConfiguration.Radius = _controller.radius;

            _environmentQuery = new EnvironmentQuery(_environmentQueryConfiguration);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            var characterCenter = transform.position + _controller.center;

            _environmentQuery.Update(characterCenter);

            _motor.Begin();

            if (!Mathf.Approximately(_rotation.sqrMagnitude, 0.0f))
            {
                _motor.ApplyRotation(_rotation);
            }

            if (_controller.isGrounded)
            {
                _motor.Applyground(_environmentQuery.GroundNormal);

                _motor.ApplyMovement(_environmentQuery.GroundNormal, _movement);

                if (_jump)
                {
                    _motor.ApplyJump();
                    _jump = false;
                }
                else
                {
                    if (_interact)
                    {
                        //Get interactive from _environmentQuery and do something with it
                        _interact = false;
                    }
                }

            }

            _motor.Commit();
        }


        public void Jump()
        {
            _jump = true;
        }

        public void Interact()
        {
            _interact = true;
        }

        public void Move(Vector3 worldDirection)
        {
            _movement = worldDirection;
        }

        public void Rotate(Vector3 worldDirection)
        {
            _rotation = worldDirection;
        }
    }
}
