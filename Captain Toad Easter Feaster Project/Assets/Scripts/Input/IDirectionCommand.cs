﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Input
{
    public interface IDirectionalCommand
    {
        void Execute(Vector2 direction);
    }
}