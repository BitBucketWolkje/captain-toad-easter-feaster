﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Input
{
    public class InputHandler
    {
        public IImpulseCommand ACommand;

        public IImpulseCommand BCommand;

        public IDirectionalCommand RightJoyStickCommand;

        public IDirectionalCommand LeftJoyStickCommand;

        public void Update()
        {

            if (UnityEngine.Input.GetButtonDown("A"))
            {
                ACommand.Execute();
            }

            if (UnityEngine.Input.GetButtonDown("B"))
            {
                BCommand.Execute();
            }


            float horizontalRight = UnityEngine.Input.GetAxis("RightStick_Horizontal");
            float verticalRight = UnityEngine.Input.GetAxis("RightStick_Vertical");

            Vector2 directionRight = new Vector2(horizontalRight, verticalRight);

            directionRight = directionRight.sqrMagnitude > 1 ? directionRight.normalized : directionRight;

            RightJoyStickCommand.Execute(directionRight);




            //get left stick input
            float horizontalLeft = UnityEngine.Input.GetAxis("LeftStick_Horizontal");
            float verticalLeft = UnityEngine.Input.GetAxis("LeftStick_Vertical");

            // calculate the player movement direction
            Vector2 directionLeft = new Vector2(horizontalLeft, verticalLeft);

            LeftJoyStickCommand.Execute(directionLeft);


        }

    }
}