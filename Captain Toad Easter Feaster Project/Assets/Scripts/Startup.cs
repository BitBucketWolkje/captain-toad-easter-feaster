﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game;
using Input;
using UnityEngine;


public class Startup : MonoBehaviour
{
    private InputHandler _inputHandler;


    [SerializeField]
    private Camera _camera;

    [SerializeField]
    private PlayerBehaviour _player;

    public void Start()
    {
        _inputHandler = new InputHandler();

        _inputHandler.ACommand = new JumpCommand(_player);
        _inputHandler.BCommand = new InteractCommand(_player);
        _inputHandler.RightJoyStickCommand = new RotateCommand(_player, _camera);
        _inputHandler.LeftJoyStickCommand = new MoveCommand(_player, _camera);

    }

    private void Update()
    {
        _inputHandler.Update();
    }
}

